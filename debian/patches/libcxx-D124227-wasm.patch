From 67b0b02ec9f2bbc57bf8f0550828d97f460ac11f Mon Sep 17 00:00:00 2001
From: Brad Smith <brad@comstyle.com>
Date: Sat, 7 May 2022 01:06:32 -0400
Subject: [PATCH] [libcxx] Remove static inline and make use of
 _LIBCPP_HIDE_FROM_ABI in __support headers

After feedback from D122861, do the same thing with some of the other headers. Try to move the
headers so they have a similar style and way of doing things.

+ also applies:
https://reviews.llvm.org/D141208

Reviewed By: ldionne, daltenty

Differential Revision: https://reviews.llvm.org/D124227
---
 libcxx/include/__support/ibm/gettod_zos.h  |  3 +-
 libcxx/include/__support/ibm/xlocale.h     | 53 +++++++++------------
 libcxx/include/__support/musl/xlocale.h    | 31 ++++++------
 libcxx/include/__support/solaris/xlocale.h | 55 +++++++++++-----------
 4 files changed, 67 insertions(+), 75 deletions(-)

Index: llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895/libcxx/include/__support/ibm/gettod_zos.h
===================================================================
--- llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895.orig/libcxx/include/__support/ibm/gettod_zos.h
+++ llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895/libcxx/include/__support/ibm/gettod_zos.h
@@ -12,7 +12,8 @@
 
 #include <time.h>
 
-static inline int gettimeofdayMonotonic(struct timespec64* Output) {
+inline _LIBCPP_HIDE_FROM_ABI int
+gettimeofdayMonotonic(struct timespec64* Output) {
 
   // The POSIX gettimeofday() function is not available on z/OS. Therefore,
   // we will call stcke and other hardware instructions in implement equivalent.
Index: llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895/libcxx/include/__support/ibm/xlocale.h
===================================================================
--- llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895.orig/libcxx/include/__support/ibm/xlocale.h
+++ llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895/libcxx/include/__support/ibm/xlocale.h
@@ -52,57 +52,50 @@ private:
 
 // The following are not POSIX routines.  These are quick-and-dirty hacks
 // to make things pretend to work
-static inline
-long long strtoll_l(const char *__nptr, char **__endptr,
-    int __base, locale_t locale) {
+inline _LIBCPP_HIDE_FROM_ABI long long
+strtoll_l(const char *__nptr, char **__endptr, int __base, locale_t locale) {
   __setAndRestore __newloc(locale);
-  return strtoll(__nptr, __endptr, __base);
+  return ::strtoll(__nptr, __endptr, __base);
 }
 
-static inline
-long strtol_l(const char *__nptr, char **__endptr,
-    int __base, locale_t locale) {
+inline _LIBCPP_HIDE_FROM_ABI long
+strtol_l(const char *__nptr, char **__endptr, int __base, locale_t locale) {
   __setAndRestore __newloc(locale);
-  return strtol(__nptr, __endptr, __base);
+  return ::strtol(__nptr, __endptr, __base);
 }
 
-static inline
-double strtod_l(const char *__nptr, char **__endptr,
-    locale_t locale) {
+inline _LIBCPP_HIDE_FROM_ABI double
+strtod_l(const char *__nptr, char **__endptr, locale_t locale) {
   __setAndRestore __newloc(locale);
-  return strtod(__nptr, __endptr);
+  return ::strtod(__nptr, __endptr);
 }
 
-static inline
-float strtof_l(const char *__nptr, char **__endptr,
-    locale_t locale) {
+inline _LIBCPP_HIDE_FROM_ABI float
+strtof_l(const char *__nptr, char **__endptr, locale_t locale) {
   __setAndRestore __newloc(locale);
-  return strtof(__nptr, __endptr);
+  return ::strtof(__nptr, __endptr);
 }
 
-static inline
-long double strtold_l(const char *__nptr, char **__endptr,
-    locale_t locale) {
+inline _LIBCPP_HIDE_FROM_ABI long double
+strtold_l(const char *__nptr, char **__endptr, locale_t locale) {
   __setAndRestore __newloc(locale);
-  return strtold(__nptr, __endptr);
+  return ::strtold(__nptr, __endptr);
 }
 
-static inline
-unsigned long long strtoull_l(const char *__nptr, char **__endptr,
-    int __base, locale_t locale) {
+inline _LIBCPP_HIDE_FROM_ABI unsigned long long
+strtoull_l(const char *__nptr, char **__endptr, int __base, locale_t locale) {
   __setAndRestore __newloc(locale);
-  return strtoull(__nptr, __endptr, __base);
+  return ::strtoull(__nptr, __endptr, __base);
 }
 
-static inline
-unsigned long strtoul_l(const char *__nptr, char **__endptr,
-    int __base, locale_t locale) {
+inline _LIBCPP_HIDE_FROM_ABI unsigned long
+strtoul_l(const char *__nptr, char **__endptr, int __base, locale_t locale) {
   __setAndRestore __newloc(locale);
-  return strtoul(__nptr, __endptr, __base);
+  return ::strtoul(__nptr, __endptr, __base);
 }
 
-static inline
-int vasprintf(char **strp, const char *fmt, va_list ap) {
+inline _LIBCPP_HIDE_FROM_ABI int
+vasprintf(char **strp, const char *fmt, va_list ap) {
   const size_t buff_size = 256;
   if ((*strp = (char *)malloc(buff_size)) == NULL) {
     return -1;
Index: llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895/libcxx/include/__support/musl/xlocale.h
===================================================================
--- llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895.orig/libcxx/include/__support/musl/xlocale.h
+++ llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895/libcxx/include/__support/musl/xlocale.h
@@ -24,30 +24,29 @@
 extern "C" {
 #endif
 
-static inline long long strtoll_l(const char *nptr, char **endptr, int base,
-                                  locale_t) {
-  return strtoll(nptr, endptr, base);
+inline _LIBCPP_HIDE_FROM_ABI long long
+strtoll_l(const char *nptr, char **endptr, int base, locale_t) {
+  return ::strtoll(nptr, endptr, base);
 }
 
-static inline unsigned long long strtoull_l(const char *nptr, char **endptr,
-                                            int base, locale_t) {
-  return strtoull(nptr, endptr, base);
+inline _LIBCPP_HIDE_FROM_ABI unsigned long long
+strtoull_l(const char *nptr, char **endptr, int base, locale_t) {
+  return ::strtoull(nptr, endptr, base);
 }
 
-static inline long long wcstoll_l(const wchar_t *nptr, wchar_t **endptr,
-                                  int base, locale_t) {
-  return wcstoll(nptr, endptr, base);
+inline _LIBCPP_HIDE_FROM_ABI long long
+wcstoll_l(const wchar_t *nptr, wchar_t **endptr, int base, locale_t) {
+  return ::wcstoll(nptr, endptr, base);
 }
 
-static inline unsigned long long wcstoull_l(const wchar_t *nptr,
-                                            wchar_t **endptr, int base,
-                                            locale_t) {
-  return wcstoull(nptr, endptr, base);
+inline _LIBCPP_HIDE_FROM_ABI unsigned long long
+wcstoull_l(const wchar_t *nptr, wchar_t **endptr, int base, locale_t) {
+  return ::wcstoull(nptr, endptr, base);
 }
 
-static inline long double wcstold_l(const wchar_t *nptr, wchar_t **endptr,
-                                    locale_t) {
-  return wcstold(nptr, endptr);
+inline _LIBCPP_HIDE_FROM_ABI long double
+wcstold_l(const wchar_t *nptr, wchar_t **endptr, locale_t) {
+  return ::wcstold(nptr, endptr);
 }
 
 #ifdef __cplusplus
Index: llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895/libcxx/include/__support/solaris/xlocale.h
===================================================================
--- llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895.orig/libcxx/include/__support/solaris/xlocale.h
+++ llvm-toolchain-14-14.0.6~++20230104093240+f28c006a5895/libcxx/include/__support/solaris/xlocale.h
@@ -32,40 +32,39 @@ struct lconv *localeconv(void);
 struct lconv *localeconv_l(locale_t __l);
 
 // FIXME: These are quick-and-dirty hacks to make things pretend to work
-static inline
-long long strtoll_l(const char *__nptr, char **__endptr,
-    int __base, locale_t __loc) {
-  return strtoll(__nptr, __endptr, __base);
-}
-static inline
-long strtol_l(const char *__nptr, char **__endptr,
-    int __base, locale_t __loc) {
-  return strtol(__nptr, __endptr, __base);
-}
-static inline
-unsigned long long strtoull_l(const char *__nptr, char **__endptr,
-    int __base, locale_t __loc) {
-  return strtoull(__nptr, __endptr, __base);
-}
-static inline
-unsigned long strtoul_l(const char *__nptr, char **__endptr,
-    int __base, locale_t __loc) {
-  return strtoul(__nptr, __endptr, __base);
-}
-static inline
-float strtof_l(const char *__nptr, char **__endptr,
-    locale_t __loc) {
-  return strtof(__nptr, __endptr);
-}
-static inline
-double strtod_l(const char *__nptr, char **__endptr,
-    locale_t __loc) {
-  return strtod(__nptr, __endptr);
-}
-static inline
-long double strtold_l(const char *__nptr, char **__endptr,
-    locale_t __loc) {
-  return strtold(__nptr, __endptr);
+inline _LIBCPP_HIDE_FROM_ABI long long
+strtoll_l(const char *__nptr, char **__endptr, int __base, locale_t __loc) {
+  return ::strtoll(__nptr, __endptr, __base);
+}
+
+inline _LIBCPP_HIDE_FROM_ABI long
+strtol_l(const char *__nptr, char **__endptr, int __base, locale_t __loc) {
+  return ::strtol(__nptr, __endptr, __base);
+}
+
+inline _LIBCPP_HIDE_FROM_ABI unsigned long long
+strtoull_l(const char *__nptr, char **__endptr, int __base, locale_t __loc) 
+  return ::strtoull(__nptr, __endptr, __base);
+}
+
+inline _LIBCPP_HIDE_FROM_ABI unsigned long
+strtoul_l(const char *__nptr, char **__endptr, int __base, locale_t __loc) {
+  return ::strtoul(__nptr, __endptr, __base);
+}
+
+inline _LIBCPP_HIDE_FROM_ABI float
+strtof_l(const char *__nptr, char **__endptr, locale_t __loc) {
+  return ::strtof(__nptr, __endptr);
+}
+
+inline _LIBCPP_HIDE_FROM_ABI double
+strtod_l(const char *__nptr, char **__endptr, locale_t __loc) {
+  return ::strtod(__nptr, __endptr);
+}
+
+inline _LIBCPP_HIDE_FROM_ABI long double
+strtold_l(const char *__nptr, char **__endptr, locale_t __loc) {
+  return ::strtold(__nptr, __endptr);
 }
 
 
